"use strict";

// TODO Declare variables here

var myFirstNumber = 3;
var myFirstBoolean = false;
var mySecondNumber = 10.25;
var myFirstString = "Hello"
var mySecondString = "World"

var myFirstCustomNumber = myFirstNumber + 7;
var mySecondCustomNumber = mySecondNumber - myFirstNumber;
var myFirstCustomString = myFirstString + " " + mySecondString;
var myCustomStringNumber = myFirstString + myFirstNumber;
var mySecondCustomString = mySecondString - mySecondNumber;


// TODO Use console.log statements to print the values of the variables to the command line.
console.log(myFirstNumber);
console.log(myFirstBoolean);
console.log(mySecondNumber);
console.log(myFirstString);
console.log(mySecondString);

console.log(myFirstCustomNumber);
console.log(mySecondCustomNumber);
console.log(myFirstCustomString);
console.log(myCustomStringNumber);
console.log(mySecondCustomString);





